import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
    visible: boolean = true;
    isDateAgreed: boolean = false;
    userName: string = '';

    userForm = this.formBuilder.group({
        name: ['', Validators.required],
    })

    constructor(private formBuilder: FormBuilder) { };

    onUserNameSubmit() {
        this.userName = this.userForm.get('name')?.value ?? '';
        this.visible = false;
    }

    onDateAgreedClick() {
        this.isDateAgreed = true;
    }
    
    onFocus() {
        const button = document.querySelector('.btn') as HTMLElement;
        const container = document.querySelector('.container') as HTMLElement;

        const containerRect = container.getBoundingClientRect();
        const buttonRect = button.getBoundingClientRect();

        const maxX = containerRect.width - buttonRect.width;
        const maxY = containerRect.height - buttonRect.height;

        const randomX = Math.floor(Math.random() * maxX);
        const randomY = Math.floor(Math.random() * maxY);

        button.style.left = `${randomX}px`;
        button.style.top = `${randomY}px`;
    }
}
