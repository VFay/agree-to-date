import { NgModule, } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputSwitchModule } from 'primeng/inputswitch'
import { CardModule } from 'primeng/card';
import { DividerModule } from 'primeng/divider';
import { ListboxModule } from 'primeng/listbox';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { AppComponent } from './app.component';
import { ButtonModule } from "primeng/button"
import { TabMenuModule } from "primeng/tabmenu"
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DividerModule,
    ListboxModule,
    SelectButtonModule,
    TieredMenuModule,
    CardModule,
    ButtonModule,
    ScrollPanelModule,
    InputSwitchModule,
    TabMenuModule,
    DialogModule,
    ReactiveFormsModule,
    InputTextModule,
    RouterModule.forRoot([])],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: []
})

export class AppModule {}